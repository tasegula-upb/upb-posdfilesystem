package com.tasegula.upb.posd.utils;

public class Utils {
    private static final String ROOT = "root";

    public static final String DELIM = "";
    public static final String NL = "";

    public static final String WEB_DELIM = "&nbsp;";
    public static final String WEB_NL = "</p>";

    public static String trimPath(String path) {
        return path.replaceAll("[/]$", "");
    }

    public static boolean checkRoot(String username, String password) {
        return ROOT.equals(username) && ROOT.equals(password);
    }
}

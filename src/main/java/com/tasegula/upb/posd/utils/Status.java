package com.tasegula.upb.posd.utils;

public class Status {
    public static final Status OK = new Status("OK");

    public static final Status AUTH_OK = new Status("AUTH_OK");
    public static final Status AUTH_ERROR = new Status("AUTH_ERROR");

    public static final Status USER_CREATED = new Status("USER_CREATED");
    public static final Status USER_EXISTING = new Status("USER_EXISTING");

    public static final Status FILE_EXISTING = new Status("FILE_EXISTING");
    public static final Status FILE_NOT_EXIST = new Status("FILE_NOT_EXIST");

    public static final Status ROLE_EXISTING = new Status("ROLE_EXISTING");
    public static final Status ROLE_NOT_EXIST = new Status("ROLE_NOT_EXIST");
    public static final Status ROLE_CONSTRAINT = new Status("ROLE_CONSTRAINT");

    public static final Status PERMISSION_EXISTING = new Status("PERMISSION_EXISTING");
    public static final Status PERMISSION_NOT_EXIST = new Status("PERMISSION_NOT_EXIST");

    public static final Status UNKNOWN_PERMISSION = new Status("UNKNOWN_PERMISSION");

    public final String name;
    public final String message;
    public String payload;

    public Status(String name) {
        this.name = name;
        this.message = null;
    }

    public Status(String name, String payload) {
        this.name = name;
        this.message = null;
        this.payload = payload;
    }

    public Status setPayload(String payload) {
        return new Status(name, payload);
    }

    @Override
    public String toString() {
        String s = "Status{" +
                "name='" + name + '\'';
        s += (message != null) ?
                ", payload='" + message + '\'' :
                "";
        s += (payload != null) ?
                ", payload='" + payload + '\'' :
                "";

        s += '}';

        return s;
    }
}

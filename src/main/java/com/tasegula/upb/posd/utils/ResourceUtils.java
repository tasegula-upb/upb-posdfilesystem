package com.tasegula.upb.posd.utils;

import com.tasegula.upb.posd.db.model.Resource;
import com.tasegula.upb.posd.db.model.User;
import com.tasegula.upb.posd.db.repository.UserDao;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class ResourceUtils {

    @Nullable
    public static Resource getResource(UserDao userDao, String path) {
        String[] toks = path.split("/");
        if (toks.length < 2) return null;

        String currentName = toks[1];
        User owner = userDao.findByUsername(currentName);
        if (owner == null) return null;
        Resource currentResource = owner.getRoot();
        if (currentResource == null) return null;

        for (int i = 2; i < toks.length; i++) {
            // if not directory, then we can't search further down on children
            if (!currentResource.isDirectory()) return null;

            Resource resource = currentResource.getChild(toks[i]);
            if (resource == null) return null;

            currentResource = resource;
        }
        return currentResource;
    }

    @NotNull
    public static Resource createDirectory(User owner, Resource parent, String name) {
        Resource resource = new Resource(owner, parent, name);
        parent.getChildren().add(resource);
        return resource;
    }

    @NotNull
    public static Resource createFile(User owner, Resource parent, String name, String content) {
        Resource resource = new Resource(owner, parent, name, content);
        parent.getChildren().add(resource);
        return resource;
    }
}

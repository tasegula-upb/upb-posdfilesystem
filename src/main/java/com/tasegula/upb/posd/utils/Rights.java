package com.tasegula.upb.posd.utils;

public enum Rights {

    PRIVATE,
    READ,
    WRITE,
    PUBLIC,
    UNKNOWN;

    public static Rights from(String rights) {
        char[] chars = rights.toCharArray();

        if (chars.length == 0) return PRIVATE;
        if (chars.length == 1)
            if (chars[0] == 'r' || chars[0] == 'R') return READ;
            else if (chars[0] == 'w' || chars[0] == 'W') return WRITE;
        if (chars.length == 2 && rights.equalsIgnoreCase("rw")) return PUBLIC;
        else return UNKNOWN;
    }

    public static Rights from(int value) {
        if (value < 0 || value > 4) return UNKNOWN;
        return Rights.values()[value];
    }
}

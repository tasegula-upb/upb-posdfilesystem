package com.tasegula.upb.posd.utils;

import java.util.Collection;
import java.util.List;

public class ToString {

    private final StringBuilder sb;
    private final String name;
    private final String delim;
    private final String nl;

    public ToString(String name, String delim, String nl) {
        this.name = name;
        this.delim = delim;
        this.nl = nl;

        this.sb = new StringBuilder(nl);
        sb.append(name).append(": {").append(nl);
    }

    public ToString add(String key, Object value) {
        sb.append(delim).append(key).append(": ").append(value).append(", ").append(nl);
        return this;
    }

    @Override
    public String toString() {
        return sb.append("}").append(nl).toString().replaceAll(", $", "");
    }
}

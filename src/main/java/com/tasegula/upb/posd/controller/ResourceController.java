package com.tasegula.upb.posd.controller;

import com.tasegula.upb.posd.db.repository.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/resource")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    // region MAPPINGS
    @RequestMapping(method = RequestMethod.GET,
            value = "create",
            params = {"username", "password", "path", "type"})
    @ResponseBody
    public String create(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String path,
            @RequestParam int type) {

        return resourceService.create(username, password, path, type).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "create",
            params = {"username", "password", "path", "type", "content"})
    @ResponseBody
    public String create(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String path,
            @RequestParam int type,
            @RequestParam String content) {

        return resourceService.create(username, password, path, type, content).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "read",
            params = {"username", "password", "path"})
    @ResponseBody
    public String read(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String path) {

        return resourceService.read(username, password, path).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "write",
            params = {"username", "password", "path", "content"})
    @ResponseBody
    public String write(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String path,
            @RequestParam String content) {

        return resourceService.write(username, password, path, content).toString();
    }
    // endregion

}

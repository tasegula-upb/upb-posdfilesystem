package com.tasegula.upb.posd.controller;

import com.tasegula.upb.posd.db.repository.ResourceService;
import com.tasegula.upb.posd.db.repository.RightsService;
import com.tasegula.upb.posd.db.repository.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    private UserService userService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private RightsService rightsService;

    private static String ALICE = "alice";
    private static String BOB = "bob";
    private static String ROOT = "root";

    private static String READ = "r";
    private static String WRITE = "w";

    private static String FILE = "/alice/cursuri.java";

    private static String NL = "</p>";

    @RequestMapping(method = RequestMethod.GET,
            value = "")
    @ResponseBody
    public String test() {
        StringBuilder sb = new StringBuilder("<ol>");

        sb.append("<li>").append(rightsService.createRole(BOB, BOB, "role1")).append(NL);
        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role1")).append(NL);
        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role2")).append(NL);
        sb.append("<li>").append(rightsService.assignRole(BOB, "role1")).append(NL);
        sb.append("<li>").append(rightsService.assignRole(BOB, "role2")).append(NL);
        sb.append("<li>").append(rightsService.revokeRole(ROOT, ROOT, BOB, "role1")).append(NL);

        sb.append("</ol>");
        return sb.toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "tema2")
    @ResponseBody
    public String tema2() {
        StringBuilder sb = new StringBuilder("<ol>");

//        sb.append("<li>").append(rightsService.createRole(BOB, BOB, "role1")).append(NL);
//        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role1")).append(NL);
//        sb.append("<li>").append(rightsService.changeRights("role1", READ)).append(NL);
//        sb.append("<li>").append(rightsService.assignRole(BOB, "role1")).append(NL);
//
//        sb.append("<li>").append(resourceService.create(ALICE, ALICE, FILE, 1, "cursuri")).append(NL);
//        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);
//
//        sb.append("<li>").append(rightsService.addRole(BOB, BOB, FILE, "role1")).append(NL);
//        sb.append("<li>").append(rightsService.addRole(ALICE, ALICE, FILE, "role1")).append(NL);
//
//        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);
//        sb.append("<li>").append(resourceService.write(ALICE, ALICE, FILE, "cursuri2")).append(NL);
//        sb.append("<li>").append(resourceService.write(BOB, BOB, FILE, "cursuri3")).append(NL);
//
//        sb.append("<li>").append(rightsService.changeRights("role1", WRITE)).append(NL);
//
//        sb.append("<li>").append(resourceService.write(BOB, BOB, FILE, "cursuri3")).append(NL);
//        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);
//
//        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role2")).append(NL);
//        sb.append("<li>").append(rightsService.changeRights("role2", READ)).append(NL);
//        sb.append("<li>").append(rightsService.assignRole(BOB, "role2")).append(NL);
//        sb.append("<li>").append(rightsService.addRole(ALICE, ALICE, FILE, "role2")).append(NL);
//
//        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);

        sb.append("</ol>");
        return sb.toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "tema3")
    @ResponseBody
    public String tema3() {
        StringBuilder sb = new StringBuilder("<ol>");

        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role1")).append(NL);
        sb.append("<li>").append(rightsService.createPermission(BOB, BOB,"perm1", READ)).append(NL);
        sb.append("<li>").append(rightsService.createPermission(ROOT, ROOT,"perm1", READ)).append(NL);
        sb.append("<li>").append(rightsService.assignRole(ROOT, ROOT, BOB, "role1")).append(NL);

        sb.append("<li>").append(resourceService.create(ALICE, ALICE, FILE, 1, "cursuri")).append(NL);
        sb.append("<li>").append(rightsService.addPermission(BOB, BOB, FILE,  "perm1")).append(NL);

        sb.append("<li>").append(rightsService.addPermission(ALICE, ALICE, FILE,  "perm1")).append(NL);

        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);
        sb.append("<li>").append(rightsService.addPermissionToRole(ROOT, ROOT, "role1", "perm1")).append(NL);
        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);

        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role2")).append(NL);
        sb.append("<li>").append(rightsService.revokeRole(ROOT, ROOT, BOB, "role2")).append(NL);
        sb.append("<li>").append(rightsService.assignRole(ROOT, ROOT, BOB, "role2")).append(NL);
        sb.append("<li>").append(rightsService.revokeRole(ROOT, ROOT, BOB, "role2")).append(NL);

        sb.append("<li>").append(rightsService.createConstrain(ROOT, ROOT, "role1", "role2")).append(NL);
        sb.append("<li>").append(rightsService.assignRole(ROOT, ROOT, BOB, "role2")).append(NL);

        sb.append("</ol>");
        return sb.toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "tema4")
    @ResponseBody
    public String tema4() {
        StringBuilder sb = new StringBuilder("<ol>");

        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role1")).append(NL);
        sb.append("<li>").append(rightsService.createPermission(ROOT, ROOT,"perm1", READ)).append(NL);
        sb.append("<li>").append(rightsService.assignRole(ROOT, ROOT, BOB, "role1")).append(NL);

        sb.append("<li>").append(resourceService.create(ALICE, ALICE, FILE, 1, "cursuri")).append(NL);
        sb.append("<li>").append(rightsService.addPermission(ALICE, ALICE, FILE,  "perm1")).append(NL);
        sb.append("<li>").append(rightsService.addPermissionToRole(ROOT, ROOT, "role1", "perm1")).append(NL);

        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);

        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role2")).append(NL);
        sb.append("<li>").append(rightsService.revokeRole(ROOT, ROOT, BOB, "role1")).append(NL);
        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);

        sb.append("<li>").append(rightsService.assignRole(ROOT, ROOT, BOB, "role2")).append(NL);
        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);

        sb.append("<li>").append(rightsService.createHierarchy(ROOT, ROOT, "role1", "role2")).append(NL);
        sb.append("<li>").append(resourceService.read(BOB, BOB, FILE)).append(NL);

        sb.append("<li>").append(rightsService.createRole(ROOT, ROOT, "role3")).append(NL);
        sb.append("<li>").append(rightsService.createConstrain(ROOT, ROOT, "role1", "role3")).append(NL);
        sb.append("<li>").append(rightsService.assignRole(ROOT, ROOT, BOB, "role3")).append(NL);

        sb.append("</ol>");
        return sb.toString();
    }
}

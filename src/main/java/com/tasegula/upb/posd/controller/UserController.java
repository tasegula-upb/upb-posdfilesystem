package com.tasegula.upb.posd.controller;

import com.tasegula.upb.posd.db.model.User;
import com.tasegula.upb.posd.db.repository.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET,
            value = "get",
            params = {"username"})
    @ResponseBody
    public String get(
            @RequestParam String username) {
        User person = userService.findByUsername(username);
        return person.toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "create",
            params = {"username", "password"})
    @ResponseBody
    public String create(
            @RequestParam String username,
            @RequestParam String password) {
        return userService.create(username, password).toString();
    }
}

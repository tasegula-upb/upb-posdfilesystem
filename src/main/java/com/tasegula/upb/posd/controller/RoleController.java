package com.tasegula.upb.posd.controller;

import com.tasegula.upb.posd.db.model.Resource;
import com.tasegula.upb.posd.db.model.User;
import com.tasegula.upb.posd.db.repository.ResourceDao;
import com.tasegula.upb.posd.db.repository.RightsService;
import com.tasegula.upb.posd.db.repository.UserDao;
import com.tasegula.upb.posd.utils.Status;
import com.tasegula.upb.posd.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ResourceDao resourceDao;

    @Autowired
    private RightsService rightsService;

    // region MAPPINGS
    @RequestMapping(method = RequestMethod.GET,
            value = "createRole",
            params = {"username", "password", "role"})
    @ResponseBody
    public String createRole(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String role) {

        return rightsService.createRole(username, password, role).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "assignRole",
            params = {"username", "password", "user", "role"})
    @ResponseBody
    public String assignRole(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String user,
            @RequestParam String role) {

        return rightsService.assignRole(username, password, user, role).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "assignRole",
            params = {"username", "role"})
    @ResponseBody
    public String assignRole(
            @RequestParam String username,
            @RequestParam String role) {

        return rightsService.assignRole(username, role).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "revokeRole",
            params = {"username", "password", "user", "role"})
    @ResponseBody
    public String revokeRole(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String user,
            @RequestParam String role) {

        return rightsService.revokeRole(username, password, user, role).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "createConstrain",
            params = {"username", "password", "role1", "role2"})
    @ResponseBody
    public String createConstrain(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String role1,
            @RequestParam String role2) {

        return rightsService.createConstrain(username, password, role1, role2).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "addPermissionToRole",
            params = {"username", "password", "role", "permission"})
    @ResponseBody
    public String addPermissionToRole(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String role,
            @RequestParam String permission) {

        return rightsService.addPermissionToRole(username, password, role, permission).toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "createPermission",
            params = {"username", "password", "permission", "rights"})
    @ResponseBody
    public String createPermission(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String permission,
            @RequestParam String rights) {

        return rightsService.createPermission(username, password, permission, rights).toString();
    }
    // endregion

    // region GETTERS
    @RequestMapping(method = RequestMethod.GET,
            value = "getRoles",
            params = {"username"})
    @ResponseBody
    public String getRolesForUsername(
            @RequestParam String username) {

        User user = userDao.findByUsername(username);
        if (user == null) return Status.AUTH_ERROR.toString();

        return user.getRoles().toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "getPermissions",
            params = {"username"})
    @ResponseBody
    public String getPermissionsForUsername(
            @RequestParam String username) {

        User user = userDao.findByUsername(username);
        if (user == null) return Status.AUTH_ERROR.toString();

        return user.getPermissions().toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "getPermissions",
            params = {"resourceId"})
    @ResponseBody
    public String getPermissionsForResourceId(
            @RequestParam int resourceId) {

        Resource resource = resourceDao.findOne((long) resourceId);
        if (resource == null) return Status.FILE_NOT_EXIST.toString();

        return resource.getPermissions().toString();
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "getPermissions",
            params = {"resourcePath"})
    @ResponseBody
    public String getPermissionsForResourceId(
            @RequestParam String resourcePath) {

        String path = Utils.trimPath(resourcePath);

        Resource resource = resourceDao.findByPath(path);
        if (resource == null) return Status.FILE_NOT_EXIST.toString();

        return resource.getPermissions().toString();
    }
    // endregion
}

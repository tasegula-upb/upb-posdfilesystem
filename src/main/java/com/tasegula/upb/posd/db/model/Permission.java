package com.tasegula.upb.posd.db.model;

import com.tasegula.upb.posd.utils.Rights;
import com.tasegula.upb.posd.utils.ToString;
import com.tasegula.upb.posd.utils.Utils;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "permission")
public class Permission implements Pretty {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int rightsCode;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "role_has_permission",
            joinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "resource_has_permission",
            joinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "resource_id", referencedColumnName = "id"))
    private Set<Resource> resources = new HashSet<>();

    // region CONSTRUCTORS
    public Permission() {
    }

    public Permission(String name) {
        this.name = name;
        this.rightsCode = Rights.UNKNOWN.ordinal();
    }
    // endregion

    // region HIBERNATE
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRightsCode() {
        return rightsCode;
    }

    public void setRightsCode(int rightsCode) {
        this.rightsCode = rightsCode;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> users) {
        this.roles = users;
    }

    public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }
    // endregion

    public Rights getRights() {
        return Rights.from(rightsCode);
    }

    @Override
    public String toString() {
        return prettyPrint("", "");
    }

    @Override
    public String prettyPrint() {
        return prettyPrint(Utils.DELIM, Utils.NL);
    }

    public String prettyPrint(String delim, String nl) {
        final ToString ts = new ToString("Role", delim, nl);

        ts.add("id", id);
        ts.add("name", name);
        ts.add("rightsCode", rightsCode);
        ts.add("roles",
                roles.stream()
                        .map(role -> role.getId() + "-" + role.getName())
                        .collect(Collectors.toList()));

        return ts.toString();
    }
}

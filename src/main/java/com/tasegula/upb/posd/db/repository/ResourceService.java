package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.utils.Status;

public interface ResourceService {

    Status create(String username, String password, String path, int type);

    Status create(String username, String password, String path, int type, String content);

    Status read(String username, String password, String path);

    Status write(String username, String password, String path, String content);

}

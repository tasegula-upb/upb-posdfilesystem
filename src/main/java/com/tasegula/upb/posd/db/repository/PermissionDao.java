package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.Permission;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface PermissionDao extends CrudRepository<Permission, Long> {
    Permission findByName(String name);
}

package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.Resource;
import com.tasegula.upb.posd.db.model.User;
import com.tasegula.upb.posd.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ResourceDao resourceDao;

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) {
        return userDao.findByUsernameAndPassword(username, password);
    }

    @Override
    public Status create(String username, String password) {
        if (userDao.findByUsername(username) != null) return Status.USER_EXISTING;

        User user = new User(username, password);
        user = userDao.save(user);

        Resource root = new Resource(user, user.getUsername());
        root = resourceDao.save(root);

        user.setRootId(root.getId());
        user = userDao.save(user);

        return Status.USER_CREATED.setPayload(user.prettyPrint());
    }
}

package com.tasegula.upb.posd.db.model;

import com.tasegula.upb.posd.utils.Utils;
import com.tasegula.upb.posd.utils.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Entity
@Table(name = "user")
public class User implements Pretty {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private Long rootId;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Set<Resource> resources = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_has_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles = new HashSet<>();;

    // region CONSTRUCTOR
    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    // endregion

    // region HIBERNATE
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getRootId() {
        return rootId;
    }

    public void setRootId(Long rootId) {
        this.rootId = rootId;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    // endregion

    public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }

    public Resource getRoot() {
        return resources.stream().filter(new Predicate<Resource>() {
            @Override
            public boolean test(Resource resource) {
                return resource.getId().equals(rootId);
            }
        }).findFirst().orElse(null);
    }

    public Set<Role> getConstraints() {
        return getRoles().stream()
                .map(role -> role.getConstraints())
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    public Set<Permission> getPermissions() {
        return getRoles().stream()
                .map(role -> role.getPermissions())
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return prettyPrint("", "");
    }

    @Override
    public String prettyPrint() {
        return prettyPrint(Utils.DELIM, Utils.NL);
    }

    public String prettyPrint(String delim, String nl) {
        final ToString ts = new ToString("User", delim, nl);

        ts.add("id", id);
        ts.add("username", username);
        ts.add("password", password);
        ts.add("resources", getRoot());
        ts.add("roles",
                roles.stream()
                        .map(role -> role.getId() + "-" + role.getName())
                        .collect(Collectors.toList()));

        return ts.toString();
    }
}

package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface UserDao extends CrudRepository<User, Long> {

    User findByUsername(String username);
    User findByUsernameAndPassword(String username, String password);

}

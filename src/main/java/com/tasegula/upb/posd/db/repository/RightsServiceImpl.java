package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.Permission;
import com.tasegula.upb.posd.db.model.Resource;
import com.tasegula.upb.posd.db.model.Role;
import com.tasegula.upb.posd.db.model.User;
import com.tasegula.upb.posd.utils.Rights;
import com.tasegula.upb.posd.utils.Status;
import com.tasegula.upb.posd.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class RightsServiceImpl implements RightsService {

    private static final String ROOT = "root";

    @Autowired
    private UserDao userDao;

    @Autowired
    private ResourceDao resourceDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    public Status createRole(String username, String password, String roleName) {
        if (!Utils.checkRoot(username, password))
            return Status.AUTH_ERROR.setPayload("Not root user");

        if (roleDao.findByName(roleName) != null)
            return Status.ROLE_EXISTING;

        Role role = new Role(roleName);
        role = roleDao.save(role);

        return Status.OK.setPayload(role.prettyPrint());
    }

    public Status createPermission(String username, String password, String permissionName, String rights) {
        if (!Utils.checkRoot(username, password))
            return Status.AUTH_ERROR.setPayload("Not root user");

        if (permissionDao.findByName(permissionName) != null)
            return Status.PERMISSION_EXISTING;

        Permission permission = new Permission(permissionName);
        permission.setRightsCode(Rights.from(rights).ordinal());
        permission = permissionDao.save(permission);

        return Status.OK.setPayload(permission.prettyPrint());
    }

    public Status addPermissionToRole(String username, String password, String roleName, String permissionName) {
        if (!Utils.checkRoot(username, password))
            return Status.AUTH_ERROR.setPayload("Not root user");

        Role role = roleDao.findByName(roleName);
        if (role == null) return Status.ROLE_NOT_EXIST;

        Permission permission = permissionDao.findByName(permissionName);
        if (permission == null) return Status.ROLE_NOT_EXIST;

        role.getPermissions().add(permission);
        role = roleDao.save(role);

        return Status.OK.setPayload(role.prettyPrint());
    }

    public Status createConstrain(String username, String password, String roleName1, String roleName2) {
        if (!Utils.checkRoot(username, password))
            return Status.AUTH_ERROR.setPayload("Not root user");

        Role role1 = roleDao.findByName(roleName1);
        if (role1 == null) return Status.ROLE_NOT_EXIST.setPayload(roleName1);

        Role role2 = roleDao.findByName(roleName2);
        if (role2 == null) return Status.ROLE_NOT_EXIST.setPayload(roleName2);

        role1.getConstraints().add(role2);
        role2.getConstraints().add(role1);

        role1 = roleDao.save(role1);
        role2 = roleDao.save(role2);

        return Status.OK.setPayload(role1.prettyPrint() + ", " + role2.prettyPrint());
    }

    @Override
    public Status createHierarchy(String username, String password, String roleChild, String roleParent) {
        if (!Utils.checkRoot(username, password))
            return Status.AUTH_ERROR.setPayload("Not root user");

        Role childRole = roleDao.findByName(roleChild);
        if (childRole == null) return Status.ROLE_NOT_EXIST.setPayload(roleChild);

        Role parentRole = roleDao.findByName(roleParent);
        if (parentRole == null) return Status.ROLE_NOT_EXIST.setPayload(roleParent);

        parentRole.getChildren().add(childRole);

        childRole = roleDao.save(childRole);
        parentRole = roleDao.save(parentRole);

        return Status.OK.setPayload(parentRole.prettyPrint());
    }

    public Status assignRole(String username, String roleName) {
        return assignRole(ROOT, ROOT, username, roleName);
    }

    public Status assignRole(String username, String password, String userId, String roleName) {
        if (!Utils.checkRoot(username, password))
            return Status.AUTH_ERROR.setPayload("Not root user");

        User user = userDao.findByUsername(userId);
        if (user == null) return Status.AUTH_ERROR.setPayload("User not found");

        Role role = roleDao.findByName(roleName);
        if (role == null) return Status.ROLE_NOT_EXIST;

        if (user.getConstraints().contains(role))
            return Status.ROLE_CONSTRAINT.setPayload(user.getConstraints().toString());

        user.getRoles().add(role);
        user = userDao.save(user);

        return Status.OK.setPayload(user.prettyPrint());
    }

    public Status revokeRole(String username, String password, String userId, String roleName) {
        if (!Utils.checkRoot(username, password))
            return Status.AUTH_ERROR.setPayload("Not root user");

        User user = userDao.findByUsername(userId);
        if (user == null) return Status.AUTH_ERROR.setPayload("User  not found");

        Role role = roleDao.findByName(roleName);
        if (role == null)
            return Status.ROLE_NOT_EXIST;

        if (!user.getRoles().contains(role))
            return Status.ROLE_NOT_EXIST.setPayload("User does not have role");

        user.getRoles().remove(role);
        user = userDao.save(user);

        return Status.OK.setPayload(user.prettyPrint());
    }

    public Status addPermission(String username, String password, String path, String permissionName) {
        User user = userDao.findByUsernameAndPassword(username, password);
        if (user == null) return Status.AUTH_ERROR;

        path = Utils.trimPath(path);
        Resource resource = resourceDao.findByPath(path);
        if (resource == null) return Status.FILE_NOT_EXIST;

        if (resource.getOwner().getId() != user.getId())
            return Status.AUTH_ERROR.setPayload("Not the owner");

        Permission permission = permissionDao.findByName(permissionName);
        if (permission == null) return Status.PERMISSION_NOT_EXIST;

        resource.addPermission(permission);
        resource = resourceDao.save(resource);

        return Status.OK.setPayload(resource.prettyPrint());
    }

    @Override
    @Deprecated
    public Status changeRights(String roleName, String rights) {
        Role role = roleDao.findByName(roleName);
        if (role == null) return Status.ROLE_NOT_EXIST;

        role.setRightsCode(Rights.from(rights).ordinal());
        roleDao.save(role);

        return Status.OK.setPayload(role.prettyPrint());
    }
}

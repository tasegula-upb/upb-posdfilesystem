package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.utils.Status;

public interface RightsService {

    Status createRole(String username, String password, String roleName);

    Status createPermission(String username, String password, String permission, String rights);

    Status addPermissionToRole(String username, String password, String role, String permission);

    Status createConstrain(String username, String password, String role1, String role2);

    Status createHierarchy(String username, String password, String roleChild, String roleParent);

    Status assignRole(String username, String roleName);

    Status assignRole(String username, String password, String user, String roleName);

    Status revokeRole(String username, String password, String user, String roleName);

    Status addPermission(String username, String password, String path, String permission);

    @Deprecated
    Status changeRights(String roleName, String rights);
}

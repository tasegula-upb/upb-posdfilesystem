package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.Role;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface RoleDao extends CrudRepository<Role, Long> {
    Role findByName(String name);
}

package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.Resource;
import com.tasegula.upb.posd.db.model.User;
import com.tasegula.upb.posd.utils.Rights;
import com.tasegula.upb.posd.utils.ResourceUtils;
import com.tasegula.upb.posd.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.stream.Collectors;

@Service
@Transactional
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ResourceDao resourceDao;

    public Status create(String username, String password, String path, int type) {
        User user = userDao.findByUsernameAndPassword(username, password);
        if (user == null) return Status.AUTH_ERROR;

        return createResource(user, path, type, null);
    }

    public Status create(String username, String password, String path, int type, String content) {
        User user = userDao.findByUsernameAndPassword(username, password);
        if (user == null) return Status.AUTH_ERROR;

        return createResource(user, path, type, content);
    }

    public Status read(String username, String password, String path) {
        User user = userDao.findByUsernameAndPassword(username, password);
        if (user == null) return Status.AUTH_ERROR;

        return readResource(user, path);
    }

    public Status write(String username, String password, String path, String content) {
        User user = userDao.findByUsernameAndPassword(username, password);
        if (user == null) return Status.AUTH_ERROR;

        return writeResource(user, path, content);
    }

    // region HELPERS
    @NotNull
    private Status createResource(User user, String path, int type, String content) {
        String[] toks = path.split("/");
        if (toks.length < 2) return Status.FILE_EXISTING;

        String currentName = toks[1];
        User owner = userDao.findByUsername(currentName);
        if (owner == null) return Status.FILE_NOT_EXIST;
        Resource currentResource = owner.getRoot();
        if (currentResource == null) return Status.FILE_NOT_EXIST;

        for (int i = 2; i < toks.length; i++) {
            // if not directory, then we can't search further down on children
            if (!currentResource.isDirectory()) return Status.FILE_EXISTING;

            currentName = toks[i];
            Resource resource = currentResource.getChild(currentName);
            if (resource == null) {
                if (!currentResource.hasPermission(user, Rights.WRITE)) return Status.AUTH_ERROR;

                resource = (type == 0 || i != toks.length - 1) ?
                        ResourceUtils.createDirectory(owner, currentResource, currentName) :
                        ResourceUtils.createFile(owner, currentResource, currentName, content);
                resource = resourceDao.save(resource);

            } else if (i == toks.length - 1) {
                return Status.FILE_EXISTING;
            }
            currentResource = resource;
        }
        return Status.OK.setPayload(currentResource.prettyPrint());
    }

    @NotNull
    private Status readResource(User user, String path) {
        Resource resource = resourceDao.findByPath(path);
        if (resource == null) return Status.FILE_NOT_EXIST;

        if (resource.hasPermission(user, Rights.READ))
            return Status.OK.setPayload(resource.getContent());

        return Status.AUTH_ERROR.setPayload("Permission error: " +
                "USER: " + user.getPermissions().stream().map(p -> p.getName()).collect(Collectors.toList()) +
                ", RESOURCE: " + resource.getPermissions().stream().map(p -> p.getName()).collect(Collectors.toList()));
    }

    @NotNull
    private Status writeResource(User user, String path, String content) {
        Resource resource = resourceDao.findByPath(path);
        if (resource == null) return Status.FILE_NOT_EXIST;

        if (resource.hasPermission(user, Rights.WRITE)) {
            resource.setContent(content);

            resource = resourceDao.save(resource);
            return Status.OK.setPayload(resource.prettyPrint());
        }
        return Status.AUTH_ERROR;
    }
    // endregion
}

package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.Resource;
import com.tasegula.upb.posd.db.model.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Set;

@Transactional
public interface ResourceDao extends CrudRepository<Resource, Long> {

    Set<Resource> findByOwner(User owner);

    Resource findByPath(String path);

}

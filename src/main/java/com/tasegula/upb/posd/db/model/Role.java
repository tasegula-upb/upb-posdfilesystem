package com.tasegula.upb.posd.db.model;

import com.tasegula.upb.posd.utils.Rights;
import com.tasegula.upb.posd.utils.ToString;
import com.tasegula.upb.posd.utils.Utils;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Entity
@Table(name = "role")
public class Role implements Pretty {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int rightsCode;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_has_role",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private Set<User> users = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "role_has_permission",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    private Set<Permission> permissions = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "role_constraints",
            joinColumns = @JoinColumn(name = "role1", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role2", referencedColumnName = "id"))
    private Set<Role> constraints = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "role_hierarchy",
            joinColumns = @JoinColumn(name = "parent_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "child_id", referencedColumnName = "id"))
    private Set<Role> children = new HashSet<>();

    // region CONSTRUCTORS
    public Role() {
    }

    public Role(String name) {
        this.name = name;
        this.rightsCode = Rights.UNKNOWN.ordinal();
    }
    // endregion

    // region HIBERNATE
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRightsCode() {
        return rightsCode;
    }

    public void setRightsCode(int rightsCode) {
        this.rightsCode = rightsCode;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Permission> getPermissions() {
        Set<Permission> allPermissions = permissions;

        allPermissions.addAll(
                children.stream()
                        .map(Role::getPermissions)
                        .flatMap(Set::stream)
                        .collect(Collectors.toSet()));
        return allPermissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Role> getConstraints() {
        Set<Role> allConstraints = constraints;

        allConstraints.addAll(
                children.stream()
                        .map(Role::getConstraints)
                        .flatMap(Set::stream)
                        .collect(Collectors.toSet()));
        return allConstraints;
    }

    public void setConstraints(Set<Role> constraints) {
        this.constraints = constraints;
    }

    public Set<Role> getChildren() {
        return children;
    }

    public void setChildren(Set<Role> children) {
        this.children = children;
    }

    @Nullable
    public Role getChild(String name) {
        if (name == null) return null;

        return children.stream().filter(new Predicate<Role>() {
            @Override
            public boolean test(Role role) {
                return name.equals(role.getName());
            }
        }).findFirst().orElse(null);
    }
    // endregion

    public Rights getRights() {
        return Rights.from(rightsCode);
    }

    @Override
    public String toString() {
        return prettyPrint("", "");
    }

    @Override
    public String prettyPrint() {
        return prettyPrint(Utils.DELIM, Utils.NL);
    }

    public String prettyPrint(String delim, String nl) {
        final ToString ts = new ToString("Role", delim, nl);

        ts.add("id", id);
        ts.add("name", name);
        ts.add("rightsCode", rightsCode);
        ts.add("users",
                users.stream()
                        .map(user -> user.getId() + "-" + user.getUsername())
                        .collect(Collectors.toList()));

        return ts.toString();
    }
}

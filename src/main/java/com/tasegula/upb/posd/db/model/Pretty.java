package com.tasegula.upb.posd.db.model;

public interface Pretty {

    String prettyPrint();
    String prettyPrint(String delim, String nl);
}

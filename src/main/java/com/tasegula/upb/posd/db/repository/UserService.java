package com.tasegula.upb.posd.db.repository;

import com.tasegula.upb.posd.db.model.User;
import com.tasegula.upb.posd.utils.Status;

public interface UserService {
    User findByUsername(String username);
    User findByUsernameAndPassword(String username, String password);

    Status create(String username, String password);
}

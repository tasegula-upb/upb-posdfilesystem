package com.tasegula.upb.posd.db.model;

import com.tasegula.upb.posd.utils.Rights;
import com.tasegula.upb.posd.utils.ToString;
import com.tasegula.upb.posd.utils.Utils;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Entity
@Table(name = "resource")
public class Resource implements Pretty {

    private static final Long ROOT_ID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Long parentId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String path;

    @Column
    private String content;

    @Column(nullable = false)
    private Boolean isDirectory;

    @ManyToOne
    @JoinColumn(name = "owner")
    private User owner;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "resource_hierarchy",
            joinColumns = @JoinColumn(name = "parent_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "child_id", referencedColumnName = "id"))
    private Set<Resource> children = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "resource_has_permission",
            joinColumns = @JoinColumn(name = "resource_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    private Set<Permission> permissions = new HashSet<>();

    // region CONSTRUCTORS
    public Resource() {
    }

    public Resource(User owner, String name) {
        this.owner = owner;
        this.parentId = ROOT_ID;
        this.name = name;
        this.isDirectory = true;
        this.path = "/" + name;
    }

    public Resource(User owner, Resource parent, String name) {
        this.owner = owner;
        this.parentId = parent.getId();
        this.name = name;
        this.permissions = new HashSet<>(parent.getPermissions());
        this.isDirectory = true;
        this.path = parent.getPath() + "/" + name;
    }

    public Resource(User owner, Resource parent, String name, String content) {
        this.owner = owner;
        this.parentId = parent.getId();
        this.name = name;
        this.permissions = new HashSet<>(parent.getPermissions());
        this.isDirectory = false;
        this.path = parent.getPath() + "/" + name;
        this.content = content;
    }
    // endregion

    // region HIBERNATE
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContent() {
        if (!isDirectory()) return content;

        return children.stream()
                .map(resource -> resource.getName())
                .collect(Collectors.toList())
                .toString();
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(Boolean directory) {
        isDirectory = directory;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
    // endregion

    public void addPermission(Permission permission) {
        getPermissions().add(permission);

        children.stream().forEach(p -> p.addPermission(permission));
    }

    public Set<Resource> getChildren() {
        return children;
    }

    public void setChildren(Set<Resource> children) {
        this.children = children;
    }

    @Nullable
    public Resource getChild(String name) {
        if (name == null) return null;

        return children.stream().filter(new Predicate<Resource>() {
            @Override
            public boolean test(Resource resource) {
                return name.equals(resource.getName());
            }
        }).findFirst().orElse(null);
    }

    public boolean hasPermission(User user, Rights rights) {
        if (owner.getId().equals(user.getId())) return true;

        // test direct permission first: is it public?
        List<Permission> commonPermissions = getPermissions().stream()
                // keep only those permissions that are for both resource and user
                .filter(user.getPermissions()::contains)
                // keep only those permissions that are the required rights or public
                .filter(permission -> {
                    Rights permissionRights = permission.getRights();
                    return permissionRights == Rights.PUBLIC ||
                            permissionRights == rights;
                })
                .collect(Collectors.toList());

        return commonPermissions.size() > 0;
    }

    @Override
    public String toString() {
        return prettyPrint("", "");
    }

    @Override
    public String prettyPrint() {
        return prettyPrint(Utils.DELIM, Utils.NL);
    }

    @Override
    public String prettyPrint(String delim, String nl) {
        final ToString ts = new ToString("Resource", delim, nl);

        ts.add("id", id);
        ts.add("parentId", parentId);
        ts.add("name", name);
        ts.add("isDirectory", isDirectory);
        ts.add("path", path);
        ts.add("content", getContent());
        ts.add("owner", owner.getId());
        ts.add("children", children.stream().map(Object::toString).collect(Collectors.joining(nl)));
        ts.add("permissions", permissions.stream()
                .map(p -> p.getId() + "-" + p.getName())
                .collect(Collectors.joining(nl)));

        return ts.toString();
    }
}

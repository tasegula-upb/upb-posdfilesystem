package com.tasegula.upb;

import com.tasegula.upb.tema1.resources.Directory;
import com.tasegula.upb.tema1.resources.Resource;
import com.tasegula.upb.tema1.resources.User;
import com.tasegula.upb.tema1.server.RestServer;
import com.tasegula.upb.tema1.server.Status;

public class Test {
    private static final RestServer server = RestServer.getInstance();

    public static void main(String[] args) {
        server.createUser("alice", "alice");
        server.createUser("bob", "bob");

        User alice = server.getDatabase().getUser("alice");
        User bob = server.getDatabase().getUser("bob");

        System.out.println("Alice: " + alice);
        System.out.println("Bob: " + bob);

        Status[] statuses = new Status[15];

        statuses[0] = server.createResource("alice", "alice", "/bob/cursuri", 0);
        statuses[1] = server.createResource("alice", "alice", "/alice/cursuri", 0);
        statuses[2] = server.createResource("alice", "alice", "/alice/cursuri", 0);
        statuses[3] = server.createResource("alice", "alice", "/alice/cursuri", 1);

        statuses[4] = server.readResource("bob", "bob", "/alice/cursuri");
        statuses[5] = server.readResource("alice", "alice", "/alice/cursuri");

        statuses[6] = server.changeRights("bob", "bob", "/alice/cursuri", "rw");
        statuses[7] = server.changeRights("alice", "alice", "/alice/cursuri", "rw");
        statuses[8] = server.readResource("bob", "bob", "/alice/cursuri");

        statuses[9] = server.createResource("alice", "alice", "/alice/cursuri/a.java", 1, "Test");
        statuses[10] = server.readResource("bob", "bob", "/alice/cursuri");
        statuses[11] = server.readResource("bob", "bob", "/alice/cursuri/a.java");

        statuses[12] = server.changeRights("alice", "alice", "/alice/cursuri/b.java", "rw");
        statuses[13] = server.changeRights("alice", "alice", "/alice/cursuri/a.java", "");
        statuses[14] = server.readResource("bob", "bob", "/alice/cursuri/a.java");

        for (int i = 0; i < statuses.length; i++) {
            System.out.println("[" + (i + 1) + "] " + statuses[i]);
        }

        System.out.println(print("*", "\n"));
    }

    public static String print(String delim, String nl) {
        StringBuilder sb = new StringBuilder();

        for (User user : server.getDatabase().getUsers()) {
            sb.append(user.username + nl);
            sb.append(printDirectory(user.getRoot(), delim, nl) + nl);
        }

        return sb.toString();
    }

    private static String printDirectory(Directory directory, String delim, String nl) {
        StringBuilder sb = new StringBuilder(delim + directory.getName() + nl);

        for (Resource resource : directory.getChildren()) {
            if (resource.isDirectory())
                sb.append(delim + printDirectory((Directory) resource, delim + delim, nl) + nl);
            else
                sb.append(delim + resource + nl);
        }
        return sb.toString();
    }
}

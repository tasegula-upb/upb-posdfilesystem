package com.tasegula.upb.tema1.resources;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.List;

public interface Resource {

    User getOwner();

    Directory getParent();

    String getPath();

    String getName();

    String getContent();

    void setContent(String content);

    @Nullable
    List<Resource> getChildren();

    @Nullable
    Resource getChild(String name);

    boolean canBeRead();
    boolean canBeWritten();
    boolean hasAccess(User user, Permission permission);

    void setPermission(Permission permission);

    boolean isFile();

    boolean isDirectory();

}

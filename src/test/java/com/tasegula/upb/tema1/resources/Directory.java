package com.tasegula.upb.tema1.resources;


import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.ArrayList;
import java.util.List;

public class Directory extends AbstractResource {

    private final List<Resource> children;

    public Directory(Directory parent, String name) {
        super(parent, name, false);

        children = new ArrayList<Resource>();
    }

    public String getContent() {
        StringBuilder sb = new StringBuilder();
        for (Resource child : children) {
            sb.append(child.getName());
            sb.append("\n");
        }
        return sb.toString();
    }

    public void setContent(String content) {

    }

    @NonNull
    public List<Resource> getChildren() {
        return children;
    }

    @Nullable
    public Resource getChild(String name) {
        for (Resource resource: children) {
            if (resource.getName().equals(name))
                return resource;
        }
        return null;
    }

    public boolean contains(String filename) {
        return pathFor(filename) != null;
    }

    @Nullable
    public String pathFor(String filename) {
        for (Resource resource: children) {
            if (resource.isFile() && resource.getName().equals(filename))
                return resource.getPath();
            if (resource.isDirectory()) {
                String path = ((Directory) resource).pathFor(filename);
                if (path != null) return path;
            }
        }
        return null;
    }
}

package com.tasegula.upb.tema1.resources;

public class User {

    public final String username;
    public final String password;

    private Root root;

    public User(String username, String password) {
        User.assertNotNull(username, password);

        this.username = username;
        this.password = password;
    }

    public void attachRoot(Root root) {
        System.out.println("Attaching root: " + root + " to user " + username);
        if (this.root == null)
            this.root = root;
        else throw new IllegalStateException("root already attached to user: " + username + "\n root: " + this.root);
    }

    public Root getRoot() {
        return root;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
//        result = 31 * result + password.hashCode();
        return result;
    }

    public static boolean assertNotNull(String username, String password) {
        if (username == null || password == null) {
            throw new IllegalArgumentException("Username and password must not be null");
        }
        return true;
    }

    @Override
    public String toString() {
        return "User: " + username + "/" + password + "\n" +
                "Root: " + root;
    }
}

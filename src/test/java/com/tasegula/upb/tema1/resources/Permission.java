package com.tasegula.upb.tema1.resources;

public enum Permission {

    READ,
    WRITE,
    PUBLIC,
    PRIVATE,
    UNKNOWN;

    public static Permission from(String rights) {
        char[] chars = rights.toCharArray();

        if (chars.length == 0) return PRIVATE;
        if (chars.length == 1)
            if (chars[0] == 'r' || chars[0] == 'R') return READ;
            else if (chars[0] == 'w' || chars[0] == 'W') return WRITE;
        if (chars.length == 2 && rights.equalsIgnoreCase("rw")) return PUBLIC;
        else return UNKNOWN;
    }
}

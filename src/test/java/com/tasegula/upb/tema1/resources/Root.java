package com.tasegula.upb.tema1.resources;

public class Root extends Directory {

    private final User owner;

    public Root(User owner) {
        super(null, owner.username);
        this.owner = owner;
    }

    @Override
    public String getPath() {
        return "/" + getName();
    }

    @Override
    public User getOwner() {
        return owner;
    }
}

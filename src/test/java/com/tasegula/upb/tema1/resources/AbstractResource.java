package com.tasegula.upb.tema1.resources;

public abstract class AbstractResource implements Resource {

    private final Directory parent;
    private final String name;
    private final boolean isFile;

    private Permission permission;

    protected AbstractResource(Directory parent,
                               String name,
                               boolean isFile) {
        this.parent = parent;
        this.name = name;
        this.isFile = isFile;
        this.permission = parent == null ? Permission.PRIVATE : parent.getPermission();
    }

    public User getOwner() {
        return parent.getOwner();
    }

    public Directory getParent() {
        return parent;
    }

    public String getPath() {
        return parent.getPath() + "/" + name;
    }

    public String getName() {
        return name;
    }

    public Permission getPermission() {
        return permission;
    }

    public boolean canBeRead() {
        return permission == Permission.READ || permission == Permission.PUBLIC;
    }

    public boolean canBeWritten() {
        return permission == Permission.WRITE || permission == Permission.PUBLIC;
    }

    public boolean hasAccess(User user, Permission access) {
        if (getOwner().equals(user)) return true;
        if (permission == Permission.PUBLIC) return true;
        return permission == access;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public boolean isFile() {
        return isFile;
    }

    public boolean isDirectory() {
        return !isFile;
    }

    @Override
    public String toString() {
        return "AbstractResource{" +
                "parent=" + parent +
                ", name='" + name + '\'' +
                ", isFile=" + isFile +
                ", permission=" + permission +
                '}';
    }
}

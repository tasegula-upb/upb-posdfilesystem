package com.tasegula.upb.tema1.resources;


import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.List;

public class File extends AbstractResource {

    public String content;

    public File(Directory parent, String name, String content) {
        super(parent, name, true);

        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Nullable
    public List<Resource> getChildren() {
        return null;
    }

    @Nullable
    public Resource getChild(String name) {
        return null;
    }
}

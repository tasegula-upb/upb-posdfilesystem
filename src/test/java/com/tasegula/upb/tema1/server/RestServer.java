package com.tasegula.upb.tema1.server;

import com.tasegula.upb.tema1.resources.Permission;
import com.tasegula.upb.tema1.resources.User;
import org.checkerframework.checker.nullness.qual.NonNull;

public class RestServer {

    private static RestServer instance;

    private final Database database;

    private RestServer(Database database) {
        this.database = database;
    }

    public static RestServer getInstance() {
        if (instance == null) {
            instance = new RestServer(Database.getInstance());
        }
        return instance;
    }

    public Database getDatabase() {
        return database;
    }

    @NonNull
    public Status createUser(String username, String password) {
        database.addUser(username, password);
        return Status.USER_CREATED;
    }

    @NonNull
    public Status createResource(String username, String password,
                                 String path, int type) {
        return createResource(username, password, path, type, null);
    }

    @NonNull
    public Status createResource(String username, String password,
                                 String path, int type, String content) {
        if (!checkAccount(username, password)) return Status.AUTH_ERROR;

        User user = database.getUser(username);
        return database.createResource(user, path, type, content);
    }

    @NonNull
    public Status readResource(String username, String password,
                               String path) {
        if (!checkAccount(username, password)) return Status.AUTH_ERROR;

        User user = database.getUser(username);
        return database.readResource(user, path);
    }

    @NonNull
    public Status writeResource(String username, String password,
                                String path, String content) {
        if (!checkAccount(username, password)) return Status.AUTH_ERROR;

        User user = database.getUser(username);
        return database.writeResource(user, path, content);
    }

    @NonNull
    public Status changeRights(String username, String password,
                               String path, String rights) {
        if (!checkAccount(username, password)) return Status.AUTH_ERROR;

        Permission permission = Permission.from(rights);
        if (permission == Permission.UNKNOWN) return Status.UNKNOWN_PERMISSION;

        User user = database.getUser(username);
        return database.changePermission(user, path, permission);
    }


    private boolean checkAccount(String username, String password) {
        User user = database.getUser(username);
        return user.password.equals(password);
    }
}

package com.tasegula.upb.tema1.server;

import com.tasegula.upb.tema1.resources.*;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.HashSet;
import java.util.Set;

public class Database {

    private static Database instance;

    private final Set<User> users;
    private final Set<Root> roots;

    private Database() {
        this.users = new HashSet<>();
        this.roots = new HashSet<>();
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    // region User
    public Status addUser(String username, String password) {
        User.assertNotNull(username, password);

        User user = getUser(username);
        if (user == null) {
            user = new User(username, password);

            Root root = new Root(user);
            user.attachRoot(root);

            users.add(user);
            roots.add(root);
        }

        return Status.USER_CREATED;
    }

    @Nullable
    public User getUser(String username) {
        for (User user : users) {
            if (user.username.equals(username))
                return user;
        }
        return null;
    }
    // endregion

    // region Resource > create
    @NonNull
    public Status createResource(User user, String path, int type, String content) {
        String[] toks = path.split("/");
        if (toks.length < 2) return Status.FILE_EXISTING;

        String currentName = toks[1];
        Resource currentResource = getRoot(currentName);
        if (currentResource == null) return Status.FILE_NOT_EXIST;

        for (int i = 2; i < toks.length; i++) {
            // if not directory, then we can't search further down on children
            if (!currentResource.isDirectory()) return Status.FILE_EXISTING;

            currentName = toks[i];
            Resource resource = currentResource.getChild(currentName);
            if (resource == null) {
                if (!currentResource.hasAccess(user, Permission.WRITE)) return Status.AUTH_ERROR;

                resource = (type == 0 || i != toks.length - 1) ?
                        createDirectory((Directory) currentResource, currentName) :
                        createFile((Directory) currentResource, currentName, content);
            } else if (i == toks.length - 1) {
                return Status.FILE_EXISTING;
            }
            currentResource = resource;
        }
        return Status.OK;
    }

    @NonNull
    public Resource createDirectory(Directory parent, String name) {
        Resource resource = new Directory(parent, name);
        parent.getChildren().add(resource);
        return resource;
    }

    @NonNull
    public Resource createFile(Directory parent, String name, String content) {
        Resource resource = new File(parent, name, content);
        parent.getChildren().add(resource);
        return resource;
    }
    // endregion

    // region Resource > read/write
    public Status readResource(User user, String path) {
        Resource resource = getResource(path);
        if (resource == null) return Status.FILE_NOT_EXIST;

        if (resource.hasAccess(user, Permission.READ))
            return Status.OK.setPayload(resource.getContent());
        return Status.AUTH_ERROR;
    }

    public Status writeResource(User user, String path, String content) {
        Resource resource = getResource(path);
        if (resource == null) return Status.FILE_NOT_EXIST;

        if (resource.hasAccess(user, Permission.WRITE)) {
            resource.setContent(content);
            return Status.OK.setPayload(resource.getContent());
        }
        return Status.AUTH_ERROR;
    }
    // endregion

    // region Resource > permission
    public Status changePermission(User user, String path, Permission permission) {
        Resource resource = getResource(path);

        if (resource == null) return Status.FILE_NOT_EXIST;
        if (!resource.getOwner().equals(user)) return Status.AUTH_ERROR;

        resource.setPermission(permission);
        return Status.OK;
    }
    // endregion

    // region Resource > helpers
    @Nullable
    public Resource getResource(String path) {
        String[] toks = path.split("/");
        if (toks.length < 2) return null;

        Resource currentResource = getRoot(toks[1]);
        if (currentResource == null) return null;

        for (int i = 2; i < toks.length; i++) {
            // if not directory, then we can't search further down on children
            if (!currentResource.isDirectory()) return null;

            Resource resource = currentResource.getChild(toks[i]);
            if (resource == null) return null;

            currentResource = resource;
        }

        return currentResource;
    }

    @Nullable
    public Root getRoot(@NonNull String name) {
        for (Root root : roots) {
            if (root.getName().equals(name))
                return root;
        }
        return null;
    }
    // endregion

    @Nullable
    public Set<User> getUsers() {
        return users;
    }

    @Nullable
    public Set<Root> getRoots() {
        return roots;
    }
}

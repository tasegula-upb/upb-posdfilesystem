package com.tasegula.upb.tema1;

import com.tasegula.upb.tema1.server.RestServer;

//@Controller
//@EnableAutoConfiguration
public class AppString {

    public static final RestServer server = RestServer.getInstance();
//
//    @RequestMapping("/")
//    @ResponseBody
//    String home() {
//        return "Hello World!";
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//            params = {"username", "password", "path", "type"})
//    @ResponseBody
//    public String createResource(
//            @RequestParam String username,
//            @RequestParam String password,
//            @RequestParam String path,
//            @RequestParam int type) {
//        return server.createResource(username, password, path, type).toString();
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//            params = {"username", "password", "path", "type", "content"})
//    @ResponseBody
//    public String createResource(
//            @RequestParam String username,
//            @RequestParam String password,
//            @RequestParam String path,
//            @RequestParam int type,
//            @RequestParam String content) {
//        return server.createResource(username, password, path, type, content).toString();
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//            params = {"username", "password", "path"})
//    @ResponseBody
//    public String readResource(
//            @RequestParam String username,
//            @RequestParam String password,
//            @RequestParam String path) {
//        return server.readResource(username, password, path).toString();
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//            params = {"username", "password", "path", "content"})
//    @ResponseBody
//    public String writeResource(
//            @RequestParam String username,
//            @RequestParam String password,
//            @RequestParam String path,
//            @RequestParam String content) {
//        return server.writeResource(username, password, path, content).toString();
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//            params = {"username", "password", "path", "rights"})
//    @ResponseBody
//    public String changeRights(
//            @RequestParam String username,
//            @RequestParam String password,
//            @RequestParam String path,
//            @RequestParam String rights) {
//        return server.changeRights(username, password, path, rights).toString();
//    }
//
//    @RequestMapping(method = RequestMethod.GET)
//    @ResponseBody
//    public String print() {
//        StringBuilder sb = new StringBuilder();
//
//        for (User user : server.getDatabase().getUsers()) {
//            sb.append(user.username + "<p/>");
//            sb.append(printDirectory(user.getRoot(), "*") + "<p/>");
//        }
//
//        return sb.toString();
//    }
//
//    private String printDirectory(Directory directory, String D) {
//        StringBuilder sb = new StringBuilder(D + directory.getName() + "<p/>");
//
//        for (Resource resource : directory.getChildren()) {
//            if (resource.isDirectory())
//                sb.append(D + printDirectory((Directory) resource, D + "*") + "<p/>");
//            else
//                sb.append(D + resource + "<p/>");
//        }
//        return sb.toString();
//    }
//
//    public static void main(String[] args) throws Exception {
//        init();
//
//        SpringApplication.run(AppString.class, args);
//    }
//
//    public static void init() {
//        server.createUser("alice", "alice");
//        server.createUser("bob", "bob");
//
//        User alice = server.getDatabase().getUser("alice");
//        User bob = server.getDatabase().getUser("bob");
//
//        System.out.println("Alice: " + alice);
//        System.out.println("Bob: " + bob);
//    }
}
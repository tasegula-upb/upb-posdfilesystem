INSERT INTO `posd`.`user` (`username`, `password`, `root_id`) VALUES ('root', 'root', '1');
INSERT INTO `posd`.`user` (`username`, `password`, `root_id`) VALUES ('alice', 'alice', '2');
INSERT INTO `posd`.`user` (`username`, `password`, `root_id`) VALUES ('bob', 'bob', '3');

INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`) 
		VALUES ('/',       '1', '1', '/');
INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`) 
		VALUES ('alice',   '2', '1', '/alice');
INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`)
		VALUES ('bob',     '3', '1', '/bob');

INSERT INTO `posd`.`resource_hierarchy` (`parent_id`, `child_id`) VALUES ('1', '1');
INSERT INTO `posd`.`resource_hierarchy` (`parent_id`, `child_id`) VALUES ('1', '2');
INSERT INTO `posd`.`resource_hierarchy` (`parent_id`, `child_id`) VALUES ('1', '3');

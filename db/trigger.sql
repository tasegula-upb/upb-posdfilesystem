DELIMITER $$
CREATE
	TRIGGER `hierarchy_after_resource` AFTER INSERT 
	ON `posd`.`resource`
	FOR EACH ROW BEGIN    
		INSERT INTO hierarchy (parent_id, child_id) VALUES (NEW.parent_id, NEW.id);
    END$$
DELIMITER ;
INSERT INTO `posd`.`user` (`username`, `password`, `root_id`) VALUES ('root', 'root', '1');
INSERT INTO `posd`.`user` (`username`, `password`, `root_id`) VALUES ('alice', 'alice', '2');
INSERT INTO `posd`.`user` (`username`, `password`, `root_id`) VALUES ('bob', 'bob', '3');

INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`) 
		VALUES ('/',       '1', '1', '/');
INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`) 
		VALUES ('alice',   '2', '1', '/alice');
INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`)
		VALUES ('bob',     '3', '1', '/bob');
INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`)
		VALUES ('cursuri', '2', '2', '/alice/cursuri');
INSERT INTO `posd`.`resource` (`name`, `owner`, `parent_id`, `path`, `content`, `is_directory`) 
		VALUES ('a.java',  '2', '4', '/alice/cursuri/a.java', 'Test', '0');

INSERT INTO `posd`.`hierarchy` (`parent_id`, `child_id`) VALUES ('1', '1');
INSERT INTO `posd`.`hierarchy` (`parent_id`, `child_id`) VALUES ('1', '2');
INSERT INTO `posd`.`hierarchy` (`parent_id`, `child_id`) VALUES ('1', '3');
INSERT INTO `posd`.`hierarchy` (`parent_id`, `child_id`) VALUES ('2', '4');
INSERT INTO `posd`.`hierarchy` (`parent_id`, `child_id`) VALUES ('4', '5');

INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r1', '0');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r2', '1');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r3', '2');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r4', '3');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r5', '4');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r6', '4');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r7', '4');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r8', '4');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r9', '4');
INSERT INTO `posd`.`role` (`name`, `permission_code`) VALUES ('r10', '4');

INSERT INTO `posd`.`resource_has_role` (`resource_id`, `role_id`) VALUES ('5', '1');
INSERT INTO `posd`.`resource_has_role` (`resource_id`, `role_id`) VALUES ('5', '2');
INSERT INTO `posd`.`resource_has_role` (`resource_id`, `role_id`) VALUES ('5', '3');

INSERT INTO `posd`.`user_has_role` (`user_id`, `role_id`) VALUES ('1', '1');
INSERT INTO `posd`.`user_has_role` (`user_id`, `role_id`) VALUES ('1', '2');
INSERT INTO `posd`.`user_has_role` (`user_id`, `role_id`) VALUES ('1', '3');
INSERT INTO `posd`.`user_has_role` (`user_id`, `role_id`) VALUES ('2', '3');